import View from './view';

class FighterView extends View {
  constructor(fighter, handleClick) {
    super();

    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter, handleClick) {
    const { name, source } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const checkWrapper = this.createElement({ tagName: 'div', className: 'select-fighter_wrapper' });
    const checkName = this.createElement({ tagName: 'p', className: 'select-fighter_name' });
    checkName.innerText = 'Select ' + fighter.name;
    const checkElement = this.createElement({ tagName: 'input', className: 'select-fighter', attributes: {
      type: 'checkbox',
      name: 'fighter-' + fighter._id,
      value: fighter._id,
    }, });
    checkWrapper.append(checkElement, checkName);
    this.element = this.createElement({ tagName: 'div', className: 'fighter', attributes: {
      id: 'fighter-' + fighter._id,
    }, });
    this.element.append(imageElement, nameElement, checkWrapper);
    this.element.addEventListener('click', event => handleClick(event, fighter), false);
  }

  createName(name) {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }
}

export default FighterView;