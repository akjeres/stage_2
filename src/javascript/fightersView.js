import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import Fighter from './fighter';

var fi = JSON.parse('{"_id":"3","name":"Guile","health":45,"attack":4,"defense":3,"source":"https://66.media.tumblr.com/tumblr_lq8g3548bC1qd0wh3o1_400.gif"}');
fi.getHitPower = function(){
  let criticalHitChance = 1 + Math.random();
  
  return fi.attack * criticalHitChance;
}
fi.getBlockPower = function() {
  let dodgeChance = 1 + Math.random();
  
  return fi.defense * dodgeChance;
}

var en = JSON.parse('{"_id":"4","name":"Zangief","health":60,"attack":4,"defense":1,"source":"https://media1.giphy.com/media/nlbIvY9K0jfAA/source.gif"}');
en.getHitPower = function(){
  let criticalHitChance = 1 + Math.random();
  
  return en.attack * criticalHitChance;
}
en.getBlockPower = function() {
  let dodgeChance = 1 + Math.random();
  
  return en.defense * dodgeChance;
}

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }
  fightersDetailsMap = new Map();

  fightersArr = [];

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  handleFighterClick(event, fighter) {
    if (!this.fightersDetailsMap.get(fighter._id)) {
      this.fightersDetailsMap.set(fighter._id, fighter);
    }
    const fighterInfo = fighterService.getFighterDetails(fighter._id)
    .then(resolve => {
      let currFighterInfo = this.fightersDetailsMap.get(fighter._id);
      this.fightersDetailsMap.set(fighter._id, Object.assign(resolve, currFighterInfo));
    }, reject => {
      new Error('Failed to load fighter info');
    })
    .then(resolve => {
      switch (event.target.tagName.toLowerCase()) {
        case ('input'):
          this.hideFightButton();
          const result = this.getSingleFighter(this.fightersDetailsMap.get(fighter._id));
          if (event.target.checked) {
            if (this.fightersArr[0]) {
              if (!this.fightersArr[1]) {
                this.fightersArr[1] = result;
                event.target.nextElementSibling.innerText = 'Player 2';
              } else {
                alert('Fighters list are full');
                event.target.checked = false;
              }
            } else {
              event.target.nextElementSibling.innerText = 'Player 1';
              this.fightersArr[0] = result;
            }
          } else {
            this.clearFighter(fighter._id);
          }
          if (this.fightersArr.length > 1 && this.fightersArr.every(i => !!i)) {
            this.showFightButton();
          }
          break;
        default:
          this.showModal(fighter._id);
          break;
      }
    }, reject => {
      new Error('Failed to load fighter info');
    });
  }

  showModal(id) { 
    const fighter = this.fightersDetailsMap.get(id);
    const cond = !!(document.querySelector('.modal'));
    let elem, btn, form;
    if (cond) {
      elem = document.querySelector('.modal');
      btn = elem.querySelector('button.modal-close');
      form = elem.querySelector('form.modal-form');
    } else {
      elem = this.createElement({ tagName: 'div', className: 'modal', });
      btn = this.createElement({ tagName: 'button', className: 'modal-close', });
      btn.innerHTML = '&times;';
      form = this.createElement({ tagName: 'form', className: 'modal-form', });
    }
    form.innerHTML = `<p class='fighter-name'>${fighter['name']}</p>
      <img src='${fighter['source']}' alt='${fighter['name']}' title='${fighter['name']}'>
      <div class='input_wrapper'><p class='input_name'>Attack:</p><input type='number' step='1.0' name='attack' value='${fighter['attack']}'></div>
      <div class='input_wrapper'><p class='input_name'>Defense:</p><input type='number' step='1.0' name='defense' value='${fighter['defense']}'></div>
      <div class='input_wrapper'><p class='input_name'>Health:</p><input type='number' step='1.0' name='health' value='${fighter['health']}'></div>
      <input type='submit' name='submit' value='Save'>`;
    if (!cond) {
      elem.append(btn, form);
      this.element.appendChild(elem);
    }
    elem.addEventListener('click', (e) => {
      e.stopPropagation();
      let target = e.target;
      const container = form;
      let cond = target == container || container.contains(target); 
      if (!cond) {
        removeModal();
      }
    });
    document.addEventListener('keyup', (e) => {
      if (elem && e.keyCode == 27) {
        removeModal();
      }
    });
    form.addEventListener('submit', (e) => {
      e.preventDefault();
      let data = {};
      Array.prototype.map.call(e.target, (i) => {
        if (i['type'] == 'number') {
          if (!i['value']) i['value'] = 0;

          data[i['name']] = parseFloat(i['value']);
        }
      });
      this.fightersDetailsMap.set(id, Object.assign(fighter, data));
      elem.parentNode.querySelector('#fighter-' + id + ' input[type="checkbox"]').checked = false;
      if (this.clearFighter(id)) this.hideFightButton();
      if (confirm('Fighter\'s paranmeters have been saved.\nClose the editor?')) {
        setTimeout(() => {
          removeModal();
        }, 500);
      }
    });
    function removeModal(){
      elem.remove();
      elem  = null;
    };
  }

  showFightButton() {
    const button = this.createElement({ tagName: 'button', className: 'button-fight' });
    button.innerHTML = 'Fight!';

    this.element.appendChild(button);
    button.addEventListener('click', (e) => {
      e.preventDefault();
      this.fight(...this.fightersArr);
    });
    button.focus();
  }

  hideFightButton() {
    let button = this.element.querySelector('button.button-fight');

    if (button) {
      button.remove();
      button = null;
    }
  }

  clearFighter(id) {
    if (this.fightersArr[0] && id == this.fightersArr[0]['_id']) {
      document.querySelector(`.fighters #fighter-${id} .select-fighter_name`).innerText = 'Select ' + this.fightersArr[0]['name'];
      this.fightersArr[0] = null;
      return true;
    } 
    if (this.fightersArr[1] && id == this.fightersArr[1]['_id']) {
      document.querySelector(`.fighters #fighter-${id} .select-fighter_name`).innerText = 'Select ' + this.fightersArr[1]['name'];
      this.fightersArr[1] = null;
      return true;
    }

    return false;
  }

  getSingleFighter(obj) {
    const singleFighter = new Fighter();
    return singleFighter.getFighter(obj);
  }

  fight(hero, enemy) {
    function createArea() {
      let res = document.createElement('div');
      res.classList.add('fighting-area');
      res.innerHTML = `<div class="player player-1" id="player-${hero['_id']}">
        <h2 class="player-name">${hero['name']}</h2>
        <progress class="player-health" max="100" min="0" value="100"></progress>
        <img src="${hero['source']}" alt="${hero['name']}" class="player-logo">
      </div>
      <div class="player player-2 active" id="player-${enemy['_id']}">
        <h2 class="player-name">${enemy['name']}</h2>
        <progress class="player-health" max="100" min="0" value="100"></progress>
        <img src="${enemy['source']}" alt="${enemy['name']}" class="player-logo">
      </div>
      <h1 class="winner">The winner is <span class="winner-name"></span></h1>`;

      return res;
    }
    this.hideFightButton();
    const delay = 500;
    const elmnt = this.element;
    elmnt.style.pointerEvents = 'none';
    let fightersArr = [Object.create(hero), Object.create(enemy)];
    let area = createArea();
    elmnt.insertBefore(area, elmnt.querySelector('.fighter'));
    let self = this;
    let timerID = setTimeout(function kick(){
      if (fightersArr[0]['health'] > 0 && fightersArr[1]['health'] > 0) {
        let hp = fightersArr[0].getHitPower();
        let bp = fightersArr[1].getBlockPower();
        let subtrahend = (hp - bp) > 0 ? (hp - bp) : 0;
        fightersArr[1]['health'] -= subtrahend;
        function updateHealth(id, value) {
          const initialObj = (hero['_id'] == id) ? hero : enemy;
          const fullHealth = initialObj['health'];
          let val = value / fullHealth * 100;
          if (val < 0) val = 0;
          area.querySelector('#player-' + id + ' .player-health').value = val;

          return val;
        }
        area.querySelector('.player-1').classList.toggle('active');
        area.querySelector('.player-2').classList.toggle('active');
        updateHealth(fightersArr[1]['_id'], fightersArr[1]['health']);
        fightersArr.reverse();
        timerID = setTimeout(kick, delay);
      } else {
        clearTimeout(timerID);
        let winner = fightersArr[0]['health'] > 0 ? fightersArr[0]['name'] : fightersArr[1]['name']; 
        area.querySelector('.winner-name').innerText = winner;
        area.classList.add('active');
        if (confirm('Play again?')) {
          area.remove();
          area = null;
          self.showFightButton();
          elmnt.style.pointerEvents = '';
        }       
      }
    }, delay);
  }
}

export default FightersView;