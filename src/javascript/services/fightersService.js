import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return JSON.parse(atob(apiResult.content));
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(_id) {
    try {
      const point = `details/fighter/${_id}.json`;
      const result = await callApi(point, 'GET');

      return JSON.parse(atob(result.content));
    } catch (e) {
      throw e;
    }
  }
}

export const fighterService = new FighterService();
