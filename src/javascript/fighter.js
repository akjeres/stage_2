class Fighter {
    fighter;


    getFighter(fighter) {
        fighter.getHitPower = function(){
            let criticalHitChance = 1 + Math.random();
            
            return fighter.attack * criticalHitChance;
        }
        fighter.getBlockPower = function() {
            let dodgeChance = 1 + Math.random();
            
            return fighter.defense * dodgeChance;
        }
        return fighter;
    }
}

export default Fighter;